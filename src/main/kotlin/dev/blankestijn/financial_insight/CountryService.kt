package dev.blankestijn.financial_insight

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import io.ktor.client.response.HttpResponse
import io.ktor.features.BadRequestException
import io.ktor.http.URLProtocol
import org.slf4j.LoggerFactory
import java.util.*

class CountryService {
    val log = LoggerFactory.getLogger(CountryService::class.java)
    val client = HttpClient(Apache){
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }

    suspend fun getAllCountries(): List<Country> {
        log.info("Calling all countries")
        val list = client.get<List<Country>>("https://restcountries.eu/rest/v2/all")
        log.info("Received all countries: ${list.size}")
        return list
    }

    suspend fun getCountries(currency: Currency): List<Country> {
        val response = client.get<HttpResponse> {
            this.url {
                protocol = URLProtocol.HTTPS
                host = "restcountries.eu"
                port = 443
                path("rest", "v2", "currency", currency.currencyCode)
            }
        }
        return when {
            response.call.response.status.value < 300 -> response.receive()
            response.call.response.status.value == 404 -> throw BadRequestException("Currency $currency does not exist")
            else -> throw IllegalStateException("Received unknown status code ${response.call.response.status.value}")
        }
    }

}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Country(
    val name: String,
    val topLevelDomain: List<String>,
    val alpha2Code: String,
    val alpha3Code: String,
    val currencies: List<CCurrency>
)

data class CCurrency(
    val code: String?,
    val name: String?,
    val symbol: String?
)