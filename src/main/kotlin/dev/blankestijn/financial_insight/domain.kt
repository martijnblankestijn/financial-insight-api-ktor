package dev.blankestijn.financial_insight

import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

enum class AccountType {
    Checking, Savings
}

data class MonetaryAmount(val amount: BigDecimal, val currency: Currency)

data class Account(
    val id: UUID,
    val number: String,
    val type: AccountType,
    val startingBalance: MonetaryAmount,
    val createdAt: LocalDateTime,
    val openedAt: LocalDate?,
    val name: String? = null
)
