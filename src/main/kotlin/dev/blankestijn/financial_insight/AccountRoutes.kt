package dev.blankestijn.financial_insight

import io.ktor.application.call
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.Created
import io.ktor.request.receive
import io.ktor.request.uri
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


fun Route.accounts(accountsRepository: AccountRepository, countryService: CountryService) {
    route("accounts") {
        get {
            call.respond(accountsRepository.allAccounts())
        }
        post {
            suspend fun saveAccount(openAccount: OpenAccount, accountsRepository: AccountRepository): Account {
                val account = Account(
                    id = UUID.randomUUID(),
                    number = openAccount.accountNumber,
                    type = openAccount.accountType,
                    startingBalance = openAccount.startingBalance,
                    createdAt = LocalDateTime.now(),
                    openedAt = openAccount.openedAt,
                    name = openAccount.name
                )
                accountsRepository.newAccount(account)
                call.application.environment.log.debug("Account $account saved")
                return account
            }


            val openAccount = call.receive<OpenAccount>()
            call.application.environment.log.debug("Receiving a new account $openAccount")
            val l = countryService.getCountries(openAccount.startingBalance.currency)
            when {
                l.isEmpty() -> call.respond(BadRequest, "Currency ${openAccount.startingBalance.currency} is not valid")
                openAccount.accountNumber.subSequence(0,2) in l.map { it.alpha2Code } -> {
                    val account = saveAccount(openAccount, accountsRepository)



                    call.response.header("Location", call.request.uri + "/${account.id}")
                    call.respond(Created)
                }
                else -> call.respond(BadRequest, "Currency ${openAccount.startingBalance.currency} is not valid for the " +
                        "supplied country in IBAN ${openAccount.accountNumber}")
            }
//            val countries = countryService.getAllCountries()
//            countries.exists(openAccount.startingBalance.currency)
//                ?.let {
//                }
//                ?: call.respond(BadRequest, "Currency ${openAccount.startingBalance.currency} is not valid")
        }
    }
}

fun List<Country>.exists(currency: Currency) =
    this.find { currency.currencyCode in it.currencies.map { c -> c.code } }


data class OpenAccount(
    val accountNumber: String,
    val name: String?,
    val openedAt: LocalDate?,
    val accountType: AccountType,
    val startingBalance: MonetaryAmount
)