package dev.blankestijn.financial_insight

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import java.util.*

class AccountRepository {
    suspend fun allAccounts(): List<Account> {
        return dbQuery {
            Accounts.selectAll()
                .map { toAccount(it) }
        }
    }

    suspend fun newAccount(account: Account) {
        dbQuery {
            Accounts.insert { accounts ->
                accounts[id] = account.id
                accounts[number] = account.number
                accounts[type] = account.type
                accounts[openedAt] =
                    account.openedAt?.let { convert(it).toDateTimeAtStartOfDay() }
                accounts[createdAt] =
                    convert(account.createdAt).toDateTime()
                accounts[startingBalance] = account.startingBalance.amount
                accounts[currency] =
                    account.startingBalance.currency.currencyCode
                accounts[name] = account.name
            }
        }
        }
}

fun convert(date: java.time.LocalDate) = LocalDate(date.year, date.monthValue, date.dayOfMonth)
fun convert(dateTime: java.time.LocalDateTime) = LocalDateTime(
    dateTime.year,
    dateTime.monthValue,
    dateTime.dayOfMonth,
    dateTime.hour,
    dateTime.minute,
    dateTime.second,
    dateTime.second
)
fun convert(date: LocalDate) = java.time.LocalDate.of(date.year, date.monthOfYear, date.dayOfMonth)
fun convert(time: LocalTime) = java.time.LocalTime.of(time.hourOfDay, time.minuteOfHour, time.secondOfMinute)
fun convert(dt: LocalDateTime) = java.time.LocalDateTime.of(
    convert(dt.toLocalDate()),
    convert(dt.toLocalTime())
)

object Accounts : Table("account") {
    val id = uuid("id").primaryKey()
    val number = varchar("number", 34).uniqueIndex("account_number_uk")
    val name = varchar("name", 100).nullable()
    val openedAt= date("opened_at").nullable()
    val createdAt = datetime("created_at")
    val type = enumeration("account_type", AccountType::class)
    val startingBalance = decimal("starting_balance", 14, 2)
    val currency = varchar("currency", 3)
}

fun toAccount(row: ResultRow) =
    Account(
        id = row[Accounts.id],
        number = row[Accounts.number],
        type = row[Accounts.type],
        startingBalance = MonetaryAmount(
            row[Accounts.startingBalance],
            Currency.getInstance(row[Accounts.currency])
        ),
        createdAt = convert(row[Accounts.createdAt].toLocalDateTime()),
        openedAt = row[Accounts.openedAt]?.toLocalDate()?.let {
            convert(
                it
            )
        },
        name = row[Accounts.name]
    )



fun dbInit() {
    Database.connect(hikari())
}


private fun hikari(): HikariDataSource {
    val config = HikariConfig()
    config.driverClassName = "org.postgresql.Driver"
    config.jdbcUrl = "jdbc:postgresql://localhost:5498/financial_insight_ktor_db?characterEncoding=utf8"
    config.username = "local_docker_admin"
    config.password = "password"
    config.maximumPoolSize = 3
    config.isAutoCommit = false
    config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
    config.validate()
    return HikariDataSource(config)
}

suspend fun <T> dbQuery(block: () -> T): T = withContext(IO) { transaction { block() } }