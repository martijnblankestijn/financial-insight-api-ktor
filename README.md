
Inspired by [kotlin-ktor-exposed-starter](https://github.com/raharrison/kotlin-ktor-exposed-starter/blob/93cc7015dcbf18e1f29e6d863845db6a49eb708b/src/main/kotlin/service/WidgetService.kt).

Using:

- [Exposed](https://github.com/JetBrains/Exposed)
- [Ktor](https://ktor.io/)

Services used:

- [Rest countries](https://restcountries.eu/#api-endpoints-all)
