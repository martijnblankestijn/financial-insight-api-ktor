#!/usr/bin/env bash

POSTGRES_VERSION=11.3-alpine
if [[ "$#" -ne 2 ]]; then
  NAME=financial_insight_ktor_db
  PORT=5498
else
  NAME=$1
  PORT=$2
fi
RUNNING=`docker ps -a | grep $NAME | awk '{print $1}'`
IMAGE=`docker ps -a | grep $NAME | awk '{print $1}'`

if [[ -z "$RUNNING" ]]
then
    echo "$NAME is not running"
else
    echo "Stop running container $NAME : $RUNNING"
    docker stop ${RUNNING}
fi
if [[ -z "$IMAGE" ]]
then
    echo "$NAME has no image"
else
   echo "Remove image $NAME : $IMAGE"
   docker rm $IMAGE
fi
echo "Running postgres on image/database name $NAME on port $PORT"
docker run --name ${NAME} -e POSTGRES\_USER=local_docker_admin \
  -e POSTGRES\_PASSWORD=password \
  -e POSTGRES\_DB=${NAME} \
  -p ${PORT}:5432 \
  -d postgres:${POSTGRES_VERSION}
